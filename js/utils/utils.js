var app = app || {};

/**
 * Simple helpers
 * 
 */
app.utils = (function(){
    'use strict';

    function getByClass(cls, root){
        return (root||document).getElementsByClassName(cls);
    }

    function getByTag(tag, root){
        return (root||document).getElementsByTagName(tag);
    }

    function getById(id){
        return document.getElementById(id);
    }

    function click(el, handler){
        el.addEventListener('click', handler);
    }

    function hover(el, handler){
        el.addEventListener('mouseover', handler)
    }

    function leave(el, handler){
        el.addEventListener('mouseleave', handler)
    }

    function down(el, handler){
        el.addEventListener('mousedown', handler)
    }

    function up(el, handler){
        el.addEventListener('mouseup', handler)
    }

    function keyPress(el, handler){
        el.addEventListener('keydown', handler, true);

    }


    return {
        getByClass  : getByClass,
        getById     : getById,
        getByTag    : getByTag,
        click       : click,
        hover       : hover,
        leave       : leave,
        down        : down,
        up          : up,
        keyPress    : keyPress
    }


})();