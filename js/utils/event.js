var app = app || {};

/**
 * Publisher/Subscriber implementation for event handling
 *    
 */
app.Events = (function () {

    var events = {};

    //not used, left for demo purposes
    var uid = -1;

    return {

        emit: function (event, args) {

            if (!events[event]) {
                return false;
            }

            var listeners = events[event],
                len = listeners ? listeners.length : 0;

            while (len--) {
                listeners[len].func(event, args);
            }
        },

        listen: function (event, func) {

            if (!events[event]) {
                events[event] = [];
            }

            var sid = ( ++uid ).toString();
            events[event].push({
                sid: sid,
                func: func
            });

            return sid;
        },

        unlisten: function (sid) {
            for (var m in events) {
                if (events[m]) {
                    for (var i = 0, j = events[m].length; i < j; i++) {
                        if (events[m][i].sid === sid) {
                            events[m].splice(i, 1);
                            return events;
                        }
                    }
                }
            }
        }

    }

}());