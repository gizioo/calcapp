var app = app || {};

app.Calculator = (function () {
    'use strict';

    var DIGIT = 0,
        OPERATION = 1,
        EQUALS = 2;

    /**
     * Calculator model responsible for state and logic
     * @constructor
     */
    function Calculator() {
        this.result = "0";
        this.leftOperand = this.rightOperand = "0";
        this.operation = null;
        this.lastInput = EQUALS;
        this.changeSign = 1.0;
    }

    /**
     * Run when a digit or a dot is pressed
     * @param {integer|string} [digit] The digit that was pressed
     */
    Calculator.prototype.addDigit = function(digit) {
        if (this.lastInput === OPERATION){
            this.leftOperand = this.result;
            this.rightOperand = digit;
        }
        else if (this.lastInput === EQUALS){
            this.result = "0";
            this.leftOperand = "0";

            if(digit!="."){
                this.rightOperand = digit;
            }else{
                this.rightOperand += digit;
            }
            this.operation = null;
        }
        else{
            if (digit == "0"){
                if (this.result == "0"){
                    this.rightOperand = digit;

                }else{
                    this.rightOperand = this.result + digit;

                }
            }else if (digit == "."){
                if (this.result.indexOf(digit)<0){
                    this.rightOperand = this.result + digit;

                }
            }else{
                if(this.result != "0"){
                    this.rightOperand = this.result + digit;

                }else{
                    this.rightOperand = digit;

                }
            }
        }
        this.setResult( this.rightOperand );

        this.lastInput = DIGIT;
    };

   /**
     * Applies the last operation issued
     * @param {object|string} [operation] Operation taken from the button type or resolved when a keyboard key was pressed
     */
    Calculator.prototype.applyOperation = function(operation) {
        if (this.lastInput === DIGIT){
            this.calculate();
        } 
        if(typeof(operation) == "string"){
            this.operation = app.Operations.factory(operation);
        }else{
            this.operation = operation;
        }
        this.lastInput = OPERATION;        
        
    };

    /**
     * Execute last operation to calculate the result
     */
    Calculator.prototype.calculate = function () {
        if (this.operation){

            this.rightOperand = this.operation.execute(parseFloat(this.leftOperand), this.changeSign*parseFloat(this.rightOperand));

            this.setResult(this.rightOperand.toString());
            this.leftOperand = "0";
            this.operation = null
            this.lastInput = EQUALS;
        }
    };
    /**
     * Clears the data
     */
    Calculator.prototype.clear = function () {
        this.setResult("0");
        this.leftOperand = this.rightOperand = "0";
        this.operation = null
        this.lastInput = EQUALS;
    };
    /**
     * Get the result of last calculation
     */
    Calculator.prototype.getResult = function () {
        return this.result;
    };

    /**
     * Get the current operation
     */
    Calculator.prototype.getOperation = function () {
        return this.operation;
    };

    /**
     * Set the result and emit an event
     * @param {string} [value] The value of the result to be set
     */
    Calculator.prototype.setResult = function (value) {

        this.result = value;
        app.Events.emit('model/result', this.result);
    };

    return Calculator;

})();