var app = app || {};

/*
 Strategy pattern
 */
app.Operations = (function (operations) {

    operations.NONE = '';
    operations.MULTIPLY = '*';
    operations.SUBTRACT = '-';
    operations.ADD = '+';
    operations.EXPONENT = '^';
    operations.DIVIDE = '/';
    operations.DIVIDE_SEMI = ':';


    /**
     * Operations factory method
     * @param {string} [op] A parameter to determine what operation should be returned
     */
    operations.factory = function(op){

        if (op === operations.MULTIPLY){
            return new Multiply();
        }

        if (op === operations.SUBTRACT){
            return new Subtract();
        }

        if (op === operations.ADD){
            return new Add();
        }
        if (op === operations.DIVIDE || operations === operations.DIVIDE_SEMI){
            return new Divide();
        }

        if (op === operations.EXPONENT){
            return new Exponent();
        }
        return new Operation(op);
    }


    /**
     * Operation
     * @constructor
     * @param {string} [type] Set operation type
     */
    function Operation(type) {
        this.type = type || operations.NONE;
    }

    /**
     * Get the type of operation
     */
    Operation.prototype.getType = function () {
        return this.type;
    }


    /**
     * Add operation
     */
    function Add() {
        Operation.call(this, operations.ADD);
    }

    Add.prototype = new Operation();

    Add.prototype.execute = function (a, b) {
        return a + b;
    }

    /**
     * Subtract operation
     */
    function Subtract() {
        Operation.call(this, operations.SUBTRACT);
    }

    Subtract.prototype = new Operation();

    Subtract.prototype.execute = function (a, b) {
        return a - b;
    }

    /**
     * Multiply operation
     */
    function Multiply() {
        Operation.call(this, operations.MULTIPLY);
    }

    Multiply.prototype = new Operation();

    Multiply.prototype.execute = function (a, b) {
        return a * b;
    }

    /**
     * Diviide operation
     */
    function Divide() {
        Operation.call(this, operations.DIVIDE);
    }

    Divide.prototype = new Operation();

    Divide.prototype.execute = function (a, b) {
        return a / b;
    }

    /**
     * Exponentiation operation
     */
    function Exponent() {
        Operation.call(this, operations.EXPONENT);
    }

    Exponent.prototype = new Operation();

    Exponent.prototype.execute = function (a, b) {
        return Math.pow(a, b);
    }

    return operations;

})({});