var app = app || {};


app.CalculatorKey = (function () {
    //types
    CalculatorKey.DIGIT = 'digit';
    CalculatorKey.OPERATION = 'operation';
    CalculatorKey.CLEAR = 'clear';
    CalculatorKey.EQUALS = '=';


    /**
     * CalculatorKey model
     * @constructor
     * @param {string} [type] The type of a key
     * @param {string} [value] The value of a key
     */
    function CalculatorKey(type, value) {
        this.type = type;
        this.value = value;
    }

    /**
     * Get the key's type
     */
    CalculatorKey.prototype.getType = function () {
        return this.type;
    }

    /**
     * Get the key's value
     */
    CalculatorKey.prototype.getValue = function () {
        return this.value;
    }

    /**
     * Factory for creating CalculatorKey objects
     * @param {string} [type] The type of the key
     * @param {string} [value] The value of the key
     */
    CalculatorKey.factory = function (type, value) {

        if (type === CalculatorKey.DIGIT) {
            return new CalculatorKey(type, value);
        }

        if (type === CalculatorKey.CLEAR || type === CalculatorKey.EQUALS) {
            return new CalculatorKey(type, type);
        }

        if (type === CalculatorKey.OPERATION) {
            return new CalculatorKey(type, app.Operations.factory(value));
        }

    }

    return CalculatorKey;

})();