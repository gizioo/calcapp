var app = app || {};

app.Controller = (function () {
    'use strict';

    /**
     * Controller responsible for handling user input
     * @constructor
     * @param {object} [model] The model that the controller communicates with - Calculator
     * @param {object} [view] The view that the controller communicates wiht - CalculatorView
     */
    function Controller(model, view) {

        this.model = model;
        this.view = view;

        this.activeKey = null;
        this.MAX_KEY_INDEX = this.view.calcKeys.length-1;

        //define key codes
        this.MIN_NUMBER_CODE = 48;
        this.MAX_NUMBER_CODE = 57;
        this.MIN_NUMERIC_CODE = 96;
        this.MAX_NUMERIC_CODE = 105;
        this.DOT = 190;
        this.OPERATION_CODES = [56,106,107,109,111,186,187,189,191];

        this.ARROW_UP = 38;
        this.ARROW_DOWN = 40;
        this.ARROW_LEFT = 37;
        this.ARROW_RIGHT = 39;

        this.ENTER = 13;
        this.BACKSPACE = 8;
        this.DELETE = 46;

        this.DIGIT = 'digit';
        this.OPERATION = 'operation';
        this.EQUALS = 'equals';
        this.CLEAR = 'clear';
        this.ARROW = 'arrow';

        this.initEvents();
    }
    /**
     * Initializes event listeners:
     * 1. calculator key presses
     * 2. calculator key hovers for some fancy hover action
     * 3. keyboard presses
     */
    Controller.prototype.initEvents = function() {
        var self = this;

        //listen for events
        app.Events.listen('calckey/press', function (event, button) {
            self.onCalculatorKeyPress(button);

        });

        app.Events.listen('calckey/hover', function (event, button) {
            self.deactivateActiveKey();

        });

        app.Events.listen('key/press', function (event, key) {
            self.onKeyPress(key);

        });
    };

    /**
     * Fires when a calculator key is clicked/pressed
     * @param {object} [key] Reference to the key object
     * 
     */
    Controller.prototype.onCalculatorKeyPress = function (key) {

        if (key.getType() === app.CalculatorKey.DIGIT) {
            this.model.addDigit(key.getValue());

        }
        else if (key.getType() === app.CalculatorKey.OPERATION) {
            this.model.applyOperation(key.getValue());

        }
        else if (key.getType() === app.CalculatorKey.EQUALS) {
            this.model.calculate();

        }
        else {
            this.model.clear();
        }
    };

    /**
     * Fires when a keyboard key is pressed
     * @param {string} [key] The value of the key pressed
     * 
     */
    Controller.prototype.onKeyPress = function(key){
        var code = key.keyCode || key.which;
        var whatPressed = this.checkWhatPressed(code);

        switch(whatPressed){
            case this.DIGIT:                
                this.model.addDigit(key.key);
                break;

            case this.OPERATION:                
                if(key.key === "="){
                    this.model.calculate()

                }
                else {     
                    this.model.applyOperation(key.key)

                }
                break;

            case this.EQUALS:
                if(this.activeKey){
                    this.activeKey.onClick()

                }else{
                    this.model.calculate();

                }
                break;

            case this.CLEAR:
                this.model.clear()
                break;

            case this.ARROW:
                if(this.activeKey){

                    this.activeKey.removeHighlight();

                    switch(code){
                        case this.ARROW_UP:
                            this.moveUp()
                            break;

                        case this.ARROW_DOWN:
                            this.moveDown()
                            break;

                        case this.ARROW_RIGHT:
                            this.moveRight();
                            break;

                        case this.ARROW_LEFT:
                            this.moveLeft();
                            break;

                        default:
                            break;
                    }

                    this.activeKey.addHighlight();

                }else{
                    this.firstMove();                
                }
                break;

            default:
                break;  
        }

    }
    /**
     * Checks what was the type of keyboard key pressed
     * @param {integer} [code] The keyboard key code
     * 
     */
    Controller.prototype.checkWhatPressed = function(code){
        if ((this.DOT == code) || (this.MIN_NUMBER_CODE <= code && code <= this.MAX_NUMBER_CODE) || (this.MIN_NUMERIC_CODE <= code && code <= this.MAX_NUMERIC_CODE)){
            return this.DIGIT;

        }else if (this.OPERATION_CODES.indexOf(code) >= 0){
            return this.OPERATION;

        }else if (code == this.ENTER){
            return this.EQUALS;

        }else if (code == this.BACKSPACE || code == this.DELETE){
            return this.CLEAR;

        }else if(code == this.ARROW_UP || code == this.ARROW_RIGHT || code == this.ARROW_DOWN || code == this.ARROW_LEFT){
            return this.ARROW;
        }
        return null;
    }

    /**
     * If arrows are used to control the calculator sets up the starting point on the "0" key
     * 
     */
    Controller.prototype.firstMove = function(){
        this.activeKey = this.view.calcKeys[this.view.calcKeys.length-3];
        this.activeKey.addHighlight();

    }
    /**
     * If the up arrow is used, moves the focus to the upper row of keys
     * 
     */
    Controller.prototype.moveUp = function(){
        this.activeKey = this.view.calcKeys[Math.max(0,this.view.calcKeys.indexOf(this.activeKey)-4)];

    }

    /**
     * If the uown arrow is used, moves the focus to the lower row of keys
     * 
     */
    Controller.prototype.moveDown = function(){
        this.activeKey = this.view.calcKeys[Math.min(this.MAX_KEY_INDEX,this.view.calcKeys.indexOf(this.activeKey)+4)];

    }

    /**
     * If the left arrow is used, moves the focus the button or the left or last button to the right in the upper row
     * 
     */
    Controller.prototype.moveLeft = function(){
        this.activeKey = this.view.calcKeys[Math.max(0,this.view.calcKeys.indexOf(this.activeKey)-1)];

    }

    /**
     * If the right arrow is used, moves the focus the button or the right or first button to the left in the lower row
     * 
     */
    Controller.prototype.moveRight = function(){
        this.activeKey = this.view.calcKeys[Math.min(this.MAX_KEY_INDEX,this.view.calcKeys.indexOf(this.activeKey)+1)];

    }

    /**
     * Removes highlight from an active key and a local reference
     * 
     */
    Controller.prototype.deactivateActiveKey = function(){

        if(this.activeKey){

            this.activeKey.removeHighlight();
            this.activeKey = null;
        }
    }

    /**
     * Sets initial state of the model
     *
     */
    Controller.prototype.start = function () {
        this.model.clear();
    };

    return Controller;

})();