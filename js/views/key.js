var app = app || {};

app.CalculatorKeyView = (function(){
    'use strict';

    /**
     * The view for calculator keys
     * @constructor
     * @param {object} [el] the DOM element fot the model
     * @param {object} [model] the Key model
     */
    function CalculatorKeyView(el, model) {

        this.el = el;
        this.model = model;
        this.initEvents();
    }

    /**
     * Initializes the event listeners for different key intereactions
     */
    CalculatorKeyView.prototype.initEvents = function() {
        var self = this;

        app.utils.click(this.el, function () {
            self.onClick();
        });
        app.utils.hover(this.el, function () {
            self.onHover();
        });
        app.utils.leave(this.el, function () {
            self.onLeave();
        });
        app.utils.down(this.el, function () {
            self.onDown();
        });
        app.utils.up(this.el, function () {
            self.onUp();
        });
    }

    /**
     * Handle clicks
     */
    CalculatorKeyView.prototype.onClick = function () {

        app.Events.emit('calckey/press', this.model);
    };

    /** 
     * Handle mouse/keyboard events
     */
    CalculatorKeyView.prototype.addHighlight = function () {
        this.el.classList.add('hover');
    }; 

    /** 
     * Handle mouse/keyboard events
     */
    CalculatorKeyView.prototype.removeHighlight = function () {
        this.el.classList.remove('hover');
    }; 

    /** 
     * Handle mouse/keyboard events
     */
    CalculatorKeyView.prototype.onHover = function () {
        app.Events.emit('calckey/hover', this.model);
        this.el.classList.add('hover');
    };

    /** 
     * Handle mouse/keyboard events
     */
    CalculatorKeyView.prototype.onLeave = function () {
        this.el.classList.remove('hover');
        this.el.classList.remove('Calc-CalculatorKey-Down');
    };

    /** 
     * Handle mouse/keyboard events
     */
    CalculatorKeyView.prototype.onDown = function () {
        this.el.classList.add('Calc-CalculatorKey-Down');
    };

    /** 
     * Handle mouse/keyboard events
     */
    CalculatorKeyView.prototype.onUp = function () {
        this.el.classList.remove('Calc-CalculatorKey-Down');
    };
    return CalculatorKeyView;

})();