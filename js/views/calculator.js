var app = app || {};

app.CalcView = (function(){
    'use strict';

    /**
     * The main Calculator View
     * @constructor
     * @param {object} [calc] Calculator DOM root element
     * @param {object} [doc] document or winddow object for event attatchment
     */
    function CalcView(calc, doc){

        this.el = calc;

        this.document = doc;

        this.calcKeys = [];


        this.init();
    }

    /**
     * Initializes the calculator view
     */
    CalcView.prototype.init = function(){

        var
            displayEl = app.utils.getById('display', this.el),
            buttonsEls = app.utils.getByTag('button', this.el);
        
        this.displayView = new app.DisplayView( displayEl );

        this.createCalculatorKeys(buttonsEls);

        app.utils.keyPress(this.document, this.handleKeyboard);
    }
    /**
     * Creates the calculator key objects
     */
    CalcView.prototype.createCalculatorKeys = function(buttonEls){
        var i, calcKeyView, btnModel, type, value;

        for (i = 0; i < buttonEls.length; i++){

            type = buttonEls[i].getAttribute('data-type');
            value = buttonEls[i].getAttribute('data-value');

            btnModel = app.CalculatorKey.factory(type, value);

            calcKeyView = new app.CalculatorKeyView(buttonEls[i], btnModel);

            this.calcKeys.push(calcKeyView);
            
        }

    };
     /**
     * Event emitter for keyboard presses
     */
    CalcView.prototype.handleKeyboard = function(key){
        app.Events.emit('key/press', key);
    }

    return CalcView;

})();