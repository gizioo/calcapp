var app = app || {};

app.DisplayView = (function(){
    'use strict';
    var MAX_LENGTH=10;

    /**
     * The calculators display View
     * @constructor
     * @param {object} [el] the root DOM element to attach the view to
     */
    function DisplayView(el) {

        this.el = el;
        this.initEvents();
    }

    /**
     * Initializes the event listener for result changes
     */
    DisplayView.prototype.initEvents = function() {
        var self = this;

        app.Events.listen('model/result', function(event, value) {
            self.onResultChange(value);
        });
    }
    /**
     * Event handler for result changes, additionaly sets a given precision to numbers so they fit in the 'display'
     */
    DisplayView.prototype.onResultChange = function(value) {

        if(value.length >= MAX_LENGTH){
            if(Math.abs(parseFloat(value)) >= 1){
                value = parseFloat(value).toPrecision(5);
            }else{
                value = parseFloat(value).toExponential(5);
            }
        }
        this.el.innerHTML = value;
    };

    return DisplayView;

})();