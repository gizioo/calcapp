(function () {
    'use strict';

    var
        calcEl = app.utils.getById('calc'),
        documentEl = window,
        model = new app.Calculator(),

        view = new app.CalcView(calcEl, documentEl),
        controller = new app.Controller(model, view);
        controller.start();

})();